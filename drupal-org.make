; albatross_starter make file for d.o. usage
core = "7.x"
api = "2"

; +++++ Modules +++++

projects[admin_menu][version] = "3.0-rc4"
projects[admin_menu][subdir] = "contrib"

projects[filter_perms][version] = "1.x-dev"
projects[filter_perms][subdir] = "contrib"

projects[bean][version] = "1.1"
projects[bean][subdir] = "contrib"

projects[ctools][version] = "1.3"
projects[ctools][subdir] = "contrib"

projects[context][version] = "3.0-beta2"
projects[context][subdir] = "contrib"

projects[devel][version] = "1.2"
projects[devel][subdir] = "contrib"

projects[profiler_builder][version] = "1.0-rc4"
projects[profiler_builder][subdir] = "contrib"

projects[ds][version] = "2.2"
projects[ds][subdir] = "contrib"

projects[features][version] = "1.0-beta4"
projects[features][subdir] = "contrib"

projects[email][version] = "1.2"
projects[email][subdir] = "contrib"

projects[link][version] = "1.0"
projects[link][subdir] = "contrib"

projects[select_or_other][version] = "2.16"
projects[select_or_other][subdir] = "contrib"

projects[file_entity][version] = "2.0-unstable7"
projects[file_entity][subdir] = "contrib"

projects[media][version] = "2.0-unstable7"
projects[media][subdir] = "contrib"

projects[entity][version] = "1.0"
projects[entity][subdir] = "contrib"

projects[pathauto][version] = "1.0-rc2"
projects[pathauto][subdir] = "contrib"

projects[redirect][version] = "1.0-beta3"
projects[redirect][subdir] = "contrib"

projects[stringoverrides][version] = "1.8"
projects[stringoverrides][subdir] = "contrib"

projects[token][version] = "1.0-beta6"
projects[token][subdir] = "contrib"

projects[edit][version] = "1.x-dev"
projects[edit][subdir] = "contrib"

projects[google_analytics][version] = "1.2"
projects[google_analytics][subdir] = "contrib"

projects[ckeditor][version] = "1.13"
projects[ckeditor][subdir] = "contrib"

projects[extlink][version] = "1.12"
projects[extlink][subdir] = "contrib"

projects[draggableviews][version] = "1.x-dev"
projects[draggableviews][subdir] = "contrib"

projects[views][version] = "3.7"
projects[views][subdir] = "contrib"

; +++++ Themes +++++

; basic
projects[basic][type] = "theme"
projects[basic][version] = ""; TODO add version
projects[basic][subdir] = "contrib"

